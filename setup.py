from setuptools import setup


setup(
  name='TimeClock',
  version='1.0',
  packages=['TimeClock_Fl'],
  include_package_data=True,
  install_requires=[
    'Flask',
    'Flask-Login',
    'Flask-SQLAlchemy',
    'Flask-WTF',
    'itsdangerous',
    'Jinja2'
    'MarkupSafe',
    'SQLAlchemy',
    'Werkzeug',
    'WTForms'
  ],
)
