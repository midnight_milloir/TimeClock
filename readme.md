# Time Clock

A small application for storing an employee's worked hours. Made with
Python 3.6, Flask, and SQLAlchemy.

## Getting Started

Download the source code and install the requirements using either `pip install -r requirements.txt` or `python setup.py install`.

## Built With

* [Flask](http://flask.pocoo.org) - The web framework used
* [SQLAlchemy](https://www.sqlalchemy.org) - ORM

## Authors

* **Michael Miller**

## Latest Push

March 16, 2018 at 12:28am
