from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, RadioField, SelectField
from wtforms.validators import DataRequired
from . import DEPARTMENT


class LoginForm(FlaskForm):
  username = StringField('',validators=[DataRequired()])
  password = PasswordField('',validators=[DataRequired()])
  submit = SubmitField("Sign In")


class AddUserForm(FlaskForm):
  username = StringField('',validators=[DataRequired()])
  fullname = StringField('',validators=[DataRequired()])
  department = SelectField('', choices=DEPARTMENT, validators=None)
  password = PasswordField('',validators=[DataRequired()])
  retype_password = PasswordField('',validators=[DataRequired()])
  admin = RadioField('',choices=[(0,'<font size="-1">Non-Admin</font>'),(1,'<font size="-1">Admin</font>')],default=0,coerce=int)
  supervisor = RadioField('',choices=[(0,'<font size="-1">Employee</font>'),(1,'<font size="-1">Supervisor</font>')],default=0,coerce=int)
  hourly = RadioField('',choices=[(1,'<font size="-1">Hourly</font>'),(0,'<font size="-1">Salary</font>')],default=1,coerce=int)
  submit = SubmitField("Confirm")


class ResetPasswordForm(FlaskForm):
  password = PasswordField('',validators=[DataRequired()])
  retype_password = PasswordField('',validators=[DataRequired()])
  submit = SubmitField("Confirm")
