from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, StringField


class EmployeeSelectForm(FlaskForm):
  employee = StringField('Employee', validators=None)
  punch_type = SelectField('Type', choices=[('in', 'in'), ('out', 'out')], validators=None)
  submit = SubmitField('Punch')
