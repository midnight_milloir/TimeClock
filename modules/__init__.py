from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from os import path, sep
from modules.utils.config import in_production


if path.isdir('/var/www/TimeClock'):
  app = Flask(__name__, instance_path='/var/www/TimeClock/modules/static',
              template_folder='/var/www/TimeClock/modules/views/templates')
else:
  app = Flask(__name__, instance_path=path.abspath('.') + '/modules/static/',
              template_folder=path.abspath('.') + '/modules/views/templates')


class BaseConfig(object):
  DEBUG = True
  TESTING = True
  SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):
  PROJECT_ROOT = path.abspath('.') + sep
  STATIC_DIR = PROJECT_ROOT + 'modules/static/'
  SQLALCHEMY_DATABASE_URI = 'sqlite:////' + path.join(app.instance_path, 'timeclock.db')
  SQLALCHEMY_TRACK_MODIFICATION = False
  SECRET_KEY = 'DEVELOPMENT_KEY'
  DEBUG = True
  TESTING = True


class ProductionConfig(BaseConfig):
  PROJECT_ROOT = '/var/www/TimeClock/'
  STATIC_DIR = PROJECT_ROOT + 'modules/static/'
  SQLALCHEMY_DATABASE_URI = 'sqlite:////' + '/home/pi/timeclock.db'
  SQLALCHEMY_TRACK_MODIFICATION = False
  SECRET_KEY = 'BORKLADY8000'
  DEBUG = True
  TESTING = True

if in_production():
  app.config.from_object(ProductionConfig)
else:
  app.config.from_object(DevelopmentConfig)

timeclock_db = SQLAlchemy(app)
csrf = CSRFProtect(app)
csrf.init_app(app)
login_manager = LoginManager()
login_manager.init_app(app)

"""
App Route Imports
"""
import modules.controllers.page_handlers
import modules.controllers.punch_controller
import modules.controllers.login_controller
