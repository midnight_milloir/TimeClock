from modules import timeclock_db
from werkzeug.security import generate_password_hash, check_password_hash


class Employee(timeclock_db.Model):
  username = timeclock_db.Column(timeclock_db.String(80), primary_key=True, unique=True)
  fullname = timeclock_db.Column(timeclock_db.String(80))
  department = timeclock_db.Column(timeclock_db.String(80))
  password = timeclock_db.Column(timeclock_db.String(80))
  admin = timeclock_db.Column(timeclock_db.Boolean())
  supervisor = timeclock_db.Column(timeclock_db.Boolean())
  hourly = timeclock_db.Column(timeclock_db.Boolean())

  def __init__(self, username, fullname, department, password, admin, supervisor, hourly):
    self.username = username
    self.fullname = fullname
    self.department = department
    self.password = self.set_password(password)
    self.admin = admin
    self.supervisor = supervisor
    self.hourly = hourly

  def __repr__(self):
    return '<User %r>' % self.username

  def set_password(self, password):
    return generate_password_hash(password)

  def check_password(self, password):
    return check_password_hash(self.password, password)

  def is_authenticated(self):
    return True

  def is_active(self):
    return True

  def is_anonymous(self):
    return False

  def get_id(self):
    return str(self.username)
