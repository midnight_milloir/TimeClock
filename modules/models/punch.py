from modules import timeclock_db


class Punch(timeclock_db.Model):
  id = timeclock_db.Column(timeclock_db.Integer, primary_key=True)
  name = timeclock_db.Column(timeclock_db.String(150))
  time = timeclock_db.Column(timeclock_db.DateTime())
  day = timeclock_db.Column(timeclock_db.Integer)
  status = timeclock_db.Column(timeclock_db.Enum('in', 'out', name='status'))

  def __init__(self, name, time, status, custom):
    self.name = name
    self.time = time
    self.day = time.weekday()

    previous_punch = Punch.query.filter_by(
      name=name).order_by(Punch.time.desc()).first()

    if not custom:
      if previous_punch and status == previous_punch.status:
        if previous_punch.status == 'in':
          self.status = 'out'
        else:
          self.status = 'in'
      else:
        self.status = status
    else:
      self.status = status

  def __repr__(self):
    return '<Punch {} {} at {}>'.format(self.status, self.name, self.time.strftime('%m-%d-%y %H:%M'))
