from modules.models.punch import Punch
from modules.models.employee import Employee
from datetime import datetime


def admin_punch_retrieval(pay_period):
  punches = Punch.query.filter(
      Punch.time.between(
          pay_period.pay_start,
          pay_period.pay_end
      )).order_by(Punch.name.asc()).order_by(Punch.time.desc())
  return punches, punches.count()


def supervisor_punch_retrieval(pay_period, current_user):
  names = Employee.query.filter_by(department=current_user.department)
  punches = []
  for each in names:
    punch = Punch.query.filter_by(name=each.username).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time.desc())
    for element in punch:
      punches.append(element)
  return punches, len(punches)


def employee_punch_retrieval(pay_period, current_user):
  punches = Punch.query.filter_by(name=current_user.username).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time.desc())
  return punches, punches.count()


def find_punch_errors(punches, punches_length):
  punches_w_errors = []
  for idx, each in enumerate(punches):
    punches_w_errors.append([each, ""])
    if idx == 0:
      if each.status == "in" and (datetime.now() - each.time).total_seconds() > 36000:
        punches_w_errors[idx][1] = "table-warning"
    elif (each.name != punches[idx - 1].name) and each.status == "in" and (datetime.now() - each.time).total_seconds() > 36000:
      punches_w_errors[idx][1] = "table-warning"
    if idx != punches_length - 1:
      if punches[idx + 1]:
        if each.name == punches[idx + 1].name:
          if each.status == punches[idx + 1].status:
            punches_w_errors[idx][1] = "table-danger"
          if each.status == "out" and punches[idx + 1].status == "in":
            if (each.time - punches[idx + 1].time).total_seconds() > 36000:
              punches_w_errors[idx][1] = "table-warning"
    if idx != 0:
      if each.name == punches[idx - 1].name:
        if each.status == punches[idx - 1].status:
          punches_w_errors[idx][1] = "table-danger"
        if each.status == "in" and punches[idx - 1].status == "out":
          if (punches[idx - 1].time - each.time).total_seconds() > 36000:
            punches_w_errors[idx][1] = "table-warning"
  return punches_w_errors
