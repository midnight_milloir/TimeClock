from modules import timeclock_db
from modules.models.employee import Employee

"""
This module contains code to manage the timeclock.db database. The login_controller.py file
will use functions from this module to complete tasks such as querying, inserting, and
deleting from the timeclock.db database.
"""


def get_user(username):
  return Employee.query.filter_by(username=username).first()


def get_all_users():
  return Employee.query.order_by(Employee.username.asc()).all()


def get_all_users_by_department(department):
  return Employee.query.filter_by(department=department).order_by(Employee.username.asc())


def user_match(user, username):
  return user.username == username


def password_match(password_1, password_2):
  return password_1 == password_2


def add_user(user):
  timeclock_db.session.add(user)
  timeclock_db.session.commit()


def remove_user(user):
  timeclock_db.session.delete(user)
  timeclock_db.session.commit()


def change_password(user, password):
  user.password = user.set_password(password)
  timeclock_db.session.commit()


def validate_user(username, password):
  user = get_user(username)
  if user and user_match(user, username):
    if user.check_password(password):
      return [True, user]
    else:
      errors = "Wrong Password"
      return [False, errors]
  else:
    errors = "User does not exist"
    return [False, errors]


def validate_new_user(username, fullname, department, pass_1, pass_2, admin, supervisor, hourly):
  user = get_user(username)
  if user:
    errors = "User already exists"
    return [False, errors]
  else:
    if password_match(pass_1, pass_2):
      add_user(Employee(username, fullname, department, pass_1, admin, supervisor, hourly))
      return [True, '']
    else:
      errors = "Passwords do not match"
      return [False, errors]


def validate_password_change(username, pass_1, pass_2):
  if password_match(pass_1, pass_2):
    change_password(get_user(username), pass_1)
    return True
  else:
    return False
