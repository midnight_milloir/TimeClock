from datetime import datetime, timedelta
from collections import namedtuple
from modules.models.punch import Punch
from modules.models.employee import Employee


def calculate_totals(user, pay_period):
  punches = Punch.query.filter_by(name=user.username).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time)
  time = timedelta()
  total = 0
  for key, punch in enumerate(punches[1:]):
    previous_punch = punches[key]
    if punch.status == 'out' and previous_punch.status == 'in':
      time += (punch.time - previous_punch.time)
    # Calculates totals
    seconds = time.total_seconds()
    hours = int(seconds // 3600)
    minutes = int((seconds % 3600) // 60)
    total = '{}:{:02d}'.format(hours, minutes)

  return total


def calculate_totals_pay_period(pay_period):
  people = Punch.query.with_entities(Punch.name).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).distinct()
  #people = Punch.query.with_entities(Punch.name).distinct()
  total_hours = {}
  for each in people:
    total = 0
    punches = Punch.query.filter_by(name=each[0]).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time)
    time_sum = timedelta()
    for idx, punch in enumerate(punches):
      previous_punch = punches[idx - 1]
      if punch.status == 'out' and previous_punch.status == 'in':
        time_sum += punch.time - previous_punch.time
      seconds = time_sum.total_seconds()
      hours = int(seconds // 3600)
      minutes = int((seconds % 3600) // 60)
      total = '{}:{:02d}'.format(hours, minutes)
    total_hours[each[0]] = str(total)

  return total_hours


def totals_page_calculate_totals_pay_period(pay_period):
  people = Punch.query.with_entities(Punch.name).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).distinct()
  #people = Punch.query.with_entities(Punch.name).distinct()
  total_hours = {}
  for each in people:
    total = 0
    punches = Punch.query.filter_by(name=each[0]).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time)
    time_sum = timedelta()
    for idx, punch in enumerate(punches):
      previous_punch = punches[idx - 1]
      if punch.status == 'out' and previous_punch.status == 'in':
        time_sum += punch.time - previous_punch.time
      seconds = time_sum.total_seconds()
      hours = int(seconds // 3600)
      minutes = int((seconds % 3600) // 60)
      total = '{} hours and {:02d} minutes'.format(hours, minutes)
    total_hours[each[0]] = str(total)

  return total_hours


def totals_page_calculate_supervisor_totals_pay_period(pay_period, current_user):
  people = Employee.query.filter_by(department=current_user.department)
  total_hours = {}
  for each in people:
    total = 0
    punches = Punch.query.filter_by(name=each.username).filter(Punch.time.between(pay_period.pay_start, pay_period.pay_end)).order_by(Punch.time)
    time_sum = timedelta()
    for idx, punch in enumerate(punches):
      previous_punch = punches[idx - 1]
      if punch.status == 'out' and previous_punch.status == 'in':
        time_sum += punch.time - previous_punch.time
      seconds = time_sum.total_seconds()
      hours = int(seconds // 3600)
      minutes = int((seconds % 3600) // 60)
      total = '{} hours and {:02d} minutes'.format(hours, minutes)
    total_hours[each.username] = str(total)

  return total_hours


def calculate_overtime(pay_period):
  period_start = datetime.strptime(pay_period.pay_start, "%Y-%m-%d %H:%M:%S")
  period_end = datetime.strptime(pay_period.pay_end, "%Y-%m-%d %H:%M:%S")

  week_start = period_start - timedelta(days=period_start.weekday())
  week_end = (week_start + timedelta(days=7)) - timedelta(seconds=1)
  current_period_start = period_end + timedelta(seconds=1)

  current_week = namedtuple('PayPeriod', 'pay_start pay_end')

  overtime_dict = {}

  while not week_start <= current_period_start < week_end:
    current_week.pay_start = week_start
    current_week.pay_end = week_end
    totals_dict = calculate_totals_pay_period(current_week)
    for key in totals_dict:
      if not overtime_dict.get(week_start.strftime("%Y-%m-%d")):
        overtime_dict[week_start.strftime("%Y-%m-%d")] = [[key, totals_dict[key]]]
      else:
        overtime_dict[week_start.strftime("%Y-%m-%d")].append([key, totals_dict[key]])
    week_start = week_start + timedelta(days=7)
    week_end = week_end + timedelta(days=7)

  return overtime_dict
