import csv
from modules import app
from modules.models.employee import Employee
from .get_pay_period import get_last_period
from .calculate_totals import calculate_totals_pay_period, calculate_overtime


def export_employee_data():
  pay_period = get_last_period()
  pay_period_totals = calculate_totals_pay_period(pay_period)
  overtime_totals = calculate_overtime(pay_period)
  export_dict = {}
  overtime_weeks = list(overtime_totals.keys())

  for key in pay_period_totals:
    export_dict[key] = [key[1:] + ',' + key[0], pay_period_totals[key]]
    try:
      export_dict[key].append(Employee.query.filter_by(username=key).first().department)
    except AttributeError:
      pass

  flag = False
  for key in export_dict:
    for week in overtime_totals:
      for each in overtime_totals[week]:
        if key == each[0]:
          flag = True
          export_dict[key].append(each[1])
      if not flag:
        export_dict[key].append("None")
      else:
        flag = False

  try:
    with open(app.config['STATIC_DIR'] + 'employee_data.csv', 'w') as csvfile:
      writer = csv.writer(csvfile, delimiter=',')
      writer.writerow(['Total For Period', '', 'Total Hours By Week'])
      if len(overtime_weeks) == 3:
        writer.writerow(['Name', 'Total', 'Department', overtime_weeks[0], overtime_weeks[1], overtime_weeks[2]])
      else:
        writer.writerow(['Name', 'Total', 'Department', overtime_weeks[0], overtime_weeks[1]])
      for key in export_dict:
        writer.writerow(export_dict[key])
    return None
  except IOError:
    return "File exception error"
