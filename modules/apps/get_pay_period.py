from datetime import datetime, timedelta
from collections import namedtuple
from calendar import monthrange


def get_pay_date():
  PayPeriod = namedtuple('PayPeriod', 'pay_start pay_end')
  timestamp = datetime.now()
  year = timestamp.strftime("%Y")
  month = timestamp.strftime("%m")
  day = timestamp.strftime("%d")
  if int(day) < 16:
    PayPeriod.pay_start = year + '-' + month + '-01 00:00:00'
    PayPeriod.pay_end = year + '-' + month + '-15 23:59:59'
  else:
    PayPeriod.pay_start = year + '-' + month + '-16 00:00:00'
    PayPeriod.pay_end = year + '-' + month + '-' + str(monthrange(int(year), int(month))[1]) + ' 23:59:59'
  return PayPeriod


def get_archived_period(date):
  PayPeriod = namedtuple('PayPeriod', 'pay_start pay_end')
  timestamp = date
  year = timestamp.strftime("%Y")
  month = timestamp.strftime("%m")
  day = timestamp.strftime("%d")
  if int(day) < 16:
    PayPeriod.pay_start = year + '-' + month + '-01 00:00:00'
    PayPeriod.pay_end = year + '-' + month + '-15 23:59:59'
  else:
    PayPeriod.pay_start = year + '-' + month + '-16 00:00:00'
    PayPeriod.pay_end = year + '-' + month + '-' + str(monthrange(int(year), int(month))[1]) + ' 23:59:59'
  return PayPeriod


def get_current_and_last_2_periods():
  PayPeriod = namedtuple('PayPeriod', 'pay_start pay_end')

  today = datetime.now()
  first_day_of_month = today.replace(day=1)
  last_period = first_day_of_month - timedelta(days=1)

  this_year = today.strftime("%Y")
  this_month = today.strftime("%m")
  this_day = today.strftime("%d")

  last_year = last_period.strftime("%Y")
  last_month = last_period.strftime("%m")

  if int(this_day) < 16:
    PayPeriod.pay_start = last_year + '-' + last_month + '-01 00:00:00'
    PayPeriod.pay_end = this_year + '-' + this_month + '-15 23:59:59'
  else:
    PayPeriod.pay_start = last_year + '-' + last_month + '-16 00:00:00'
    PayPeriod.pay_end = this_year + '-' + this_month + '-' + str(monthrange(int(this_year), int(this_month))[1]) + ' 23:59:59'
  return PayPeriod


def get_last_period():
  PayPeriod = namedtuple('PayPeriod', 'pay_start pay_end')

  today = datetime.now()
  first_day_of_month = today.replace(day=1)
  last_period = first_day_of_month - timedelta(days=1)

  this_year = today.strftime("%Y")
  this_month = today.strftime("%m")
  this_day = today.strftime("%d")

  last_year = last_period.strftime("%Y")
  last_month = last_period.strftime("%m")

  if int(this_day) < 16:
    PayPeriod.pay_start = last_year + '-' + last_month + '-16 00:00:00'
    PayPeriod.pay_end = last_year + '-' + last_month + '-' + str(monthrange(int(last_year), int(last_month))[1]) + ' 23:59:59'
  else:
    PayPeriod.pay_start = this_year + '-' + this_month + '-01 00:00:00'
    PayPeriod.pay_end = this_year + '-' + this_month + '-15 23:59:59'
  return PayPeriod
