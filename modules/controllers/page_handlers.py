from flask import render_template
from modules import app
from modules.views.forms.employee_select_form import EmployeeSelectForm


@app.route('/')
def ready():
  form = EmployeeSelectForm()
  return render_template('/main_page.html', form=form)
