from flask import render_template, request, flash, redirect, abort
from flask_login import login_user, login_required, current_user, logout_user
from modules import app, login_manager
from modules.views.forms import login_form
from modules.models.employee import Employee
from modules.apps import login_handler


@login_manager.user_loader
def load_user(username):
  return Employee.query.filter_by(username=username).first()


@app.route('/login', methods=['GET', 'POST'])
def login():
  form = login_form.LoginForm()
  if request.method == 'GET':
    return render_template('users/login.html', form=form)
  elif request.method == 'POST':
    if form.validate_on_submit():
      result = login_handler.validate_user(form.username.data, form.password.data)
      if result[0]:
        login_user(result[1])
        return redirect('/view_totals')
      else:
        return render_template('users/login.html', form=form, errors=result[1])
    else:
      flash("Please provide credentials")
      return render_template('users/login.html', form=form)


@app.route("/logout", methods=['GET'])
@login_required
def logout():
  logout_user()
  return redirect('/')


@app.route("/settings", methods=['GET'])
@login_required
def settings():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  if current_user.admin:
    users = login_handler.get_all_users()
  else:
    users = login_handler.get_all_users_by_department(current_user.department)
  return render_template('users/settings.html', users=users)


@app.route("/add_user", methods=['GET', 'POST'])
@login_required
def register():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  form = login_form.AddUserForm()
  if request.method == 'GET':
    return render_template('users/new_user.html', form=form)
  elif request.method == 'POST':
    if form.validate_on_submit():
      if current_user.admin:
        result = login_handler.validate_new_user(form.username.data, form.fullname.data, form.department.data, form.password.data, form.retype_password.data, form.admin.data, form.supervisor.data, form.hourly.data)
      else:
        result = login_handler.validate_new_user(form.username.data, form.fullname.data, form.department.data, form.password.data, form.retype_password.data, False, False, form.hourly.data)
      if result[0]:
        flash("User added successfully")
        return redirect('/settings')
      else:
        return render_template('users/new_user.html', form=form, errors=result[1])
    else:
      flash("Please provide a username and password")
      return render_template('users/new_user.html', form=form)


@app.route("/remove_user", methods=['POST'])
@login_required
def remove_user():
  if not current_user.admin:
    abort(401)
  user = login_handler.get_user(request.form['username'])
  if current_user.username == user.username:
    return render_template('users/settings.html', users=login_handler.get_all_users(), errors="Cannot remove self")
  login_handler.remove_user(user)
  return redirect('/settings')


@app.route("/new_password", methods=['GET', 'POST'])
@login_required
def new_password():
  if not (current_user.supervisor or current_user.admin):
    form = login_form.ResetPasswordForm()
    return render_template('users/change_password.html', form=form, user=current_user.username)
  else:
    form = login_form.ResetPasswordForm()
    return render_template('users/change_password.html', form=form, user=request.form['username'])


@app.route("/change_password", methods=['POST'])
@login_required
def change_password():
  form = login_form.ResetPasswordForm()
  if form.validate_on_submit():
    result = login_handler.validate_password_change(request.form['username'], form.password.data, form.retype_password.data)
    if result:
      if not (current_user.supervisor or current_user.admin):
        return redirect('/')
      else:
        return redirect('/settings')
    else:
      flash("Passwords do not match")
      return render_template('users/change_password.html', form=form, user=request.form['username'])
  else:
    flash("Please provide a password")
    return render_template('users/change_password.html', form=form, user=request.form['username'])
