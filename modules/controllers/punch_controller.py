from flask import request, render_template, redirect, flash, send_from_directory, abort
from flask_login import login_required, current_user
from datetime import datetime
from modules import app, timeclock_db
from modules.models.punch import Punch
from modules.models.employee import Employee
from modules.apps.get_pay_period import get_pay_date, get_archived_period
from modules.apps.calculate_totals import calculate_totals, calculate_totals_pay_period, totals_page_calculate_totals_pay_period, totals_page_calculate_supervisor_totals_pay_period
from modules.apps.export_data import export_employee_data
from modules.views.forms.employee_select_form import EmployeeSelectForm
from modules.apps.login_handler import get_user, user_match
from modules.apps.punch_handler import admin_punch_retrieval, supervisor_punch_retrieval, employee_punch_retrieval, find_punch_errors


def validate_user_session(user):
  if user and user_match(user, request.form['name']) and user.hourly:
    return True
  else:
    return False


@app.route('/add_custom_punch', methods=['GET', 'POST'])
@login_required
def add_custom_punch():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  if request.method == 'POST':
    user = get_user(request.form['name'])
    if validate_user_session(user):
      timestamp_str = request.form['date'] + ' ' + request.form['time']
      current_time = datetime.now()
      try:
        reformatted_timestamp = datetime.strptime(timestamp_str, "%Y-%m-%d %I:%M%p")
      except ValueError:
        flash('Wrong Time and Date format')
        return render_template('edit.html')
      if reformatted_timestamp > current_time:
        flash('Cannot enter future time')
        return render_template('add_custom_punch.html')
      new_punch = Punch(request.form['name'], reformatted_timestamp, request.form['status'], True)
      timeclock_db.session.add(new_punch)
      timeclock_db.session.commit()
      flash("Punch added successfully")
    else:
      flash("Punch failed. If problem persists, contact your IT professional at it@shaffercollections.com")
      return render_template('add_custom_punch.html')
    return redirect('/view_totals')
  return render_template('add_custom_punch.html')


@app.route('/edit/<int:punch_id>/', methods=['GET'])
@login_required
def edit_punch(punch_id):
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  punch = Punch.query.filter_by(id=punch_id).first()
  return render_template('edit_punch.html', punch=punch)


@app.route('/edit_punch', methods=['POST'])
@login_required
def final_edit_punch():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  punch = Punch.query.filter_by(id=request.form['punch_id']).first()
  timestamp_str = request.form['date'] + ' ' + request.form['time']
  current_time = datetime.now()
  try:
    reformatted_timestamp = datetime.strptime(timestamp_str, "%Y-%m-%d %I:%M%p")
  except ValueError:
    flash('Wrong Time and Date format')
    return render_template('edit_punch.html', punch=punch)
  if reformatted_timestamp > current_time:
    flash('Cannot enter future time')
    return render_template('edit_punch.html', punch=punch)
  punch.time = reformatted_timestamp
  punch.status = request.form['status']
  timeclock_db.session.commit()
  return redirect('/view_totals')


@app.route('/delete/<int:punch_id>/', methods=['GET'])
@login_required
def delete_punch(punch_id):
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  punch = Punch.query.get(punch_id)
  timeclock_db.session.delete(punch)
  timeclock_db.session.commit()
  return redirect('/view_totals')


@app.route('/view_totals', methods=['GET'])
@login_required
def all_punches():
  pay_period = get_pay_date()
  if current_user.admin:
    punches, punches_length = admin_punch_retrieval(pay_period)
  elif current_user.supervisor:
    punches, punches_length = supervisor_punch_retrieval(pay_period, current_user)
  else:
    punches, punches_length = employee_punch_retrieval(pay_period, current_user)
  punches_w_errors = find_punch_errors(punches, punches_length)
  totals = calculate_totals(current_user, pay_period)
  pay_period_format = [datetime.strptime(pay_period.pay_start, "%Y-%m-%d %H:%M:%S"),
                       datetime.strptime(pay_period.pay_end, "%Y-%m-%d %H:%M:%S")]
  return render_template('view_current_pay_period.html', punches=punches_w_errors, totals=totals, pay_period=pay_period_format)


@app.route('/view_past_periods', methods=['GET', 'POST'])
@login_required
def past_punches():
  if request.method == 'POST':
    date = datetime.strptime(request.form['date'], "%Y-%m-%d")
    pay_period = get_archived_period(date)
    if current_user.admin:
      punches, punches_length = admin_punch_retrieval(pay_period)
    elif current_user.supervisor:
      punches, punches_length = supervisor_punch_retrieval(pay_period, current_user)
    else:
      punches, punches_length = employee_punch_retrieval(pay_period, current_user)
    punches_w_errors = find_punch_errors(punches, punches_length)
    totals = calculate_totals(current_user, pay_period)
    pay_period_format = [datetime.strptime(pay_period.pay_start, "%Y-%m-%d %H:%M:%S"),
                         datetime.strptime(pay_period.pay_end, "%Y-%m-%d %H:%M:%S")]
    return render_template('view_past_periods.html', punches=punches_w_errors, totals=totals, pay_period=pay_period_format)
  else:
    return render_template('view_past_periods.html')


@app.route('/view_all_totals', methods=['GET', 'POST'])
@login_required
def all_totals():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  if request.method == 'POST':
    date = datetime.strptime(request.form['date'], "%Y-%m-%d")
    pay_period = get_archived_period(date)
    if current_user.admin:
      employee_totals = totals_page_calculate_totals_pay_period(pay_period)
    else:
      employee_totals = totals_page_calculate_supervisor_totals_pay_period(pay_period, current_user)
    pay_period_format = [datetime.strptime(pay_period.pay_start, "%Y-%m-%d %H:%M:%S"),
                         datetime.strptime(pay_period.pay_end, "%Y-%m-%d %H:%M:%S")]
    return render_template('view_totals.html', totals=employee_totals, pay_period=pay_period_format)
  else:
    return render_template('view_totals.html')


@app.route('/add_punch', methods=['POST'])
def add_punch():
  form = EmployeeSelectForm()
  user = get_user(form.employee.data)
  if user and user_match(user, form.employee.data) and user.hourly:
    timestamp = datetime.now()
    new_punch = Punch(form.employee.data, timestamp, form.punch_type.data, False)
    timeclock_db.session.add(new_punch)
    timeclock_db.session.commit()
    flash("Punch added successfully")
  else:
    flash("Punch failed. If problem persists, contact your IT professional at it@shaffercollections.com")
  return redirect('/')


@app.route('/employee_data', methods=['GET'])
@login_required
def export_totals():
  if not (current_user.supervisor or current_user.admin):
    abort(401)
  msg = export_employee_data()
  if msg:
    flash(msg)
    return redirect('/view_totals')
  return send_from_directory(directory=app.config['STATIC_DIR'], filename='employee_data.csv')
